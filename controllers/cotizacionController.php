<?php
$datos = array();
$conditions = null;

include_once 'models/' . $urlParams[0] . '.php';

function add()
{
    
    
    
    $urlParams = getUrl();
    
    if (is_file('views/' . $urlParams[0] . '/' . $urlParams[1] . '.php')) {
        $_SESSION["view"] = 'views/' . $urlParams[0] . '/' . $urlParams[1] . '.php';
    } else {
        $_SESSION["view"] = '404.html';
    }
}

function edit($id = null)
{
    $cotizaciones = findCotizacion($id);
    
    pr($cotizaciones);
    
    $urlParams = getUrl();
    
    if (is_file('views/' . $urlParams[0] . '/' . $urlParams[1] . '.php')) {
        $_SESSION["view"] = 'views/' . $urlParams[0] . '/' . $urlParams[1] . '.php';
    } else {
        $_SESSION["view"] = '404.html';
    }
}

function save()
{
	if ($_POST["txtMoneda"] == 'Bs') {
        $transporteU = round(($_POST["txtGastoTransporte"] / $_POST["txtTasaCambio"]) * 100) / 100;

        $transporteB = round($_POST["txtGastoTransporte"] * 100) / 100;

        $administrativosU = round(($_POST["txtGastoAdministrativos"] / $_POST["txtTasaCambio"]) * 100) / 100;

        $administrativosB = round($_POST["txtGastoAdministrativos"] * 100) / 100;
    } else {
        $transporteB = round(($_POST["txtGastoTransporte"] * $_POST["txtTasaCambio"]) * 100) / 100;

        $transporteU = round($_POST["txtGastoTransporte"] * 100) / 100;

        $administrativosB = round(($_POST["txtGastoAdministrativos"] * $_POST["txtTasaCambio"]) * 100) / 100;

        $administrativosU = round($_POST["txtGastoAdministrativos"] * 100) / 100;
    }
    
    if ($_POST["txtPorcentajeFreelance"] == null) {
        $_POST["txtPorcentajeFreelance"] = 0;
    }
    
    $ExisteCotizacion = getNumeroCotizaciones($_POST["txtCodCotizacion"]);
    if ($ExisteCotizacion == 0) {
        $fields = array(
                    'cod_cotizacion', 'moneda', 'fecha', 'margen_ganancia', 'id_cliente', 'descripcion', 'transporteUs', 
                    'transporteBs', 'gastos_adminUs', 'gastos_adminBs', 'porc_freelance', 'iva_actual', 'tasa_cambio', 
                    'status', 'status1', 'sumat', 'itf', 'created_by', 'persona_contacto',
                        );
        $fieldsDb = array(
                    $_POST["txtCodCotizacion"], $_POST["txtMoneda"], $_POST["txtFecha"], $_POST["txtMargenGanancia"], 
                    $_POST["txtIdCliente"], $_POST["txtDescripcion"], $transporteU, $transporteB, $administrativosU, 
                    $administrativosB, $_POST["txtPorcentajeFreelance"], $_POST["txtIvaActual"], $_POST["txtTasaCambio"],
                    'Abierta', 'Activa', $_POST["txtSumat"], $_POST["txtItf"], '3', $_POST["txtPersonaContacto"],
                        );
        $save = guardarCotizacion($fields, $fieldsDb);
        if ($save != null) {
            $flash = array('Ocurrio un error de base de datos... intente nuevamente...', 'msg_Error');
            set(compact('flash'));
        } else {
            $_SESSION["url"] = "index";
        }
    } else {
        $flash = array('La cotización ya existe', 'msg_Error');
        set(compact('flash'));
    }
    
}

function index()
{
    $conditions = "";
    if (count($_POST)) {
        if ($_POST["txtNumeroCotizacion"] != null) {
            $conditions = " AND cod_cotizacion = '" . $_POST["txtNumeroCotizacion"] . "'";
        }
        if ($_POST["txtFechaDesde"] != null) {
            $conditions .= " AND fecha >= '" . $_POST["txtFechaDesde"] . "'";
        }
        if ($_POST["txtFechaHasta"] != null) {
            $conditions .= " AND fecha <= '" . $_POST["txtFechaHasta"] . "'";
        }
        if ($_POST["txtDescripcion"] != null) {
            $conditions .= " AND a.descripcion LIKE '%" . $_POST["txtDescripcion"] . "%'";
        }
        if ($_POST["txtCliente"] != null) {
            $conditions .= " AND b.cliente LIKE '%" . $_POST["txtCliente"] . "%'";
        }
        if ((isset($_POST["txtAbierta"])) && (!isset($_POST["txtCerrada"]))){
            $conditions .= " AND status = '" . $_POST["txtAbierta"] . "'";
        }
        if ((!isset($_POST["txtAbierta"])) && (isset($_POST["txtCerrada"]))){
            $conditions .= " AND status = '" . $_POST["txtCerrada"] . "'";
        }
        if ((isset($_POST["txtActiva"])) && (!isset($_POST["txtDescartada"]))){
            $conditions .= " AND status1 = '" . $_POST["txtActiva"] . "'";
        }
        if ((!isset($_POST["txtActiva"])) && (isset($_POST["txtDescartada"]))){
            $conditions .= " AND status1 = '" . $_POST["txtDescartada"] . "'";
        }
    }
    
    $urlParams = getUrl();
    
    $cotizaciones = getCotizaciones($conditions);
    
    set(compact('cotizaciones'));
    
    if (is_file('views/' . $urlParams[0] . '/' . $urlParams[1] . '.php')) {
        $_SESSION["view"] = 'views/' . $urlParams[0] . '/' . $urlParams[1] . '.php';
    } else {
        $_SESSION["view"] = '404.html';
    }
}

?>
