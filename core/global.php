<?php
function getUrl() 
{
    $parametros = array();
    $url = parse_url($_SERVER['REQUEST_URI']);
    foreach (explode("/", $url['path']) as $p)
    if ($p != '') { 
        $parametros[] = $p; 
    }
    return $parametros;
}

function pr($string = null)
{
    return print_r($string);
}

function redirect($url = null)
{
    header("Location: " . $url);
}

function set($one, $two = null) 
{
    global $datos;
    if (is_array($one)) {
        if (is_array($two)) {
            $data = array_combine($one, $two);
        } else {
            $data = $one;
        }
    } else {
        $data = array($one => $two);
    }
    $datos = $data + $datos;
    return $datos;
}
function foo($array)
{
    if (empty($array)) {
        return false;
    }
    return true;
}

function flash($text = array())
{
    //msg_Success
    //This is a Success message.'
    $flash = '<div class="msgbar ' . $text[1] . ' hide_onC">
                  <span class="iconsweet">=</span>
                  <p>' . $text[0] . '</p>
              </div>';
    $_SESSION["flash"] = $flash;
    //return $flash;
}
?>