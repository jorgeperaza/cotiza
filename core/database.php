<?php
function conectar()
{
    $con = mysql_connect('localhost', 'root', 'root');
    if(!$con) {
        echo mysql_error();
    } else {
        mysql_select_db('solutions');
    }
    return $con;
}
function disconnect()
{
    mysql_close();
}

function getRegistros($strSql = null)
{
    $datos = null;
    $query = mysql_query($strSql, conectar());
    while($result = mysql_fetch_assoc($query)){
       $datos[] = $result; 
    }
    return $datos;
    disconnect();
}

function countRegistros($strSql = null)
{
    $query = mysql_query($strSql, conectar());
    $datos = mysql_num_rows($query);
    return $datos;
    disconnect();
}

function saveData($fields = array(), $fieldsDb = array(), $table = null)
{
    $strSql = "INSERT INTO 
                " . $table ."
               (";
    foreach ($fields as $field) {
        $strSql .= $field . ", ";
    }
    $strSql = substr($strSql, 0, -2);
    $strSql .= ") VALUES (";
    foreach ($fieldsDb as $Db) {
        $strSql .= "'" . $Db . "', ";
    }
    $strSql = substr($strSql, 0, -2);
    $strSql .= ")";
    
    $return = query($strSql);
    return $return;
}

function query($strSql = null)
{
    $error = null;
    if(!mysql_query($strSql, conectar())){
        $error = mysql_error();
    }
    return $error;
    disconnect();
}
?>
