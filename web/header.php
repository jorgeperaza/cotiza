<?php
if($_SESSION["url"]){
    redirect($_SESSION["url"]); 
}
$title = ucwords(str_replace("_", " ",$urlParams[0]));
?>
<!DOCTYPE html>
<html>
<head>

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<title>Dreamworks - <?=$title;?></title>
<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico">
<!--Stylesheets-->
<link rel="stylesheet" href="/web/css/reset.css" />
<link rel="stylesheet" href="/web/css/main.css" />
<link rel="stylesheet" href="/web/css/typography.css" />
<link rel="stylesheet" href="/web/css/tipsy.css" />
<link rel="stylesheet" href="/web/js/cl_editor/jquery.cleditor.css" />
<link rel="stylesheet" href="/web/uploadify/uploadify.css" />
<link rel="stylesheet" href="/web/css/jquery.ui.all.css" />
<link rel="stylesheet" href="/web/css/fullcalendar.css" />
<link rel="stylesheet" href="/web/css/bootstrap.css" />
<link rel="stylesheet" href="/web/js/fancybox/jquery.fancybox-1.3.4.css" />
<link rel="stylesheet" href="/web/css/highlight.css" />
<!--[if lt IE 9]>
    <script src="/webjs/html5.js"></script>
    <![endif]-->
<!--Javascript-->
<script type="text/javascript" src="/web/js/jquery.min.js"> </script>
<script type="text/javascript" src="/web/js/highcharts.js"> </script>
<script type="text/javascript" src="/web/js/exporting.js"> </script>
<script type="text/javascript" src="/web/js/jquery.quicksand.js"> </script>
<script type="text/javascript" src="/web/js/jquery.easing.1.3.js"> </script>
<script type="text/javascript" src="/web/js/jquery.tipsy.js"> </script>
<script type="text/javascript" src="/web/js/cl_editor/jquery.cleditor.min.js"> </script>
<script type="text/javascript" src="/web/uploadify/swfobject.js"></script>
<script type="text/javascript" src="/web/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript" src="/web/js/jquery.autogrowtextarea.js"></script>
<script type="text/javascript" src="/web/js/form_elements.js"></script>
<script type="text/javascript" src="/web/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/web/js/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/web/js/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="/web/js/jquery.ui.slider.js"></script>
<script type="text/javascript" src="/web/js/jquery.ui.progressbar.js"></script>
<script type="text/javascript" src="/web/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/web/js/jquery.ui.tabs.js"></script>
<script type="text/javascript" src="/web/js/fullcalendar.js"></script>
<script type="text/javascript" src="/web/js/gcal.js"></script>
<script type="text/javascript" src="/web/js/bootstrap-modal.js"></script>
<script type="text/javascript" src="/web/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/web/js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="/web/js/highlight.js"></script>
<script type="text/javascript" src="/web/js/main.js"> </script>
<meta charset="UTF-8"></head>
<body>
<!--Header-->
<header>
    <!--Logo-->
    <div id="logo"><a href="#"><img src="/web/images/logo.png" alt="" /></a></div>
    <!--Search-->
    <div class="header_search">
        <form action="">
            <input type="text" name="search" placeholder="Search" id="ac">
            <input type="submit" value="">
        </form>
    </div>
</header>