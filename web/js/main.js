// JavaScript Document		
jQuery(document).ready(function() {
       
    /*Click en input*/
    $(".borrable").bind('dblclick', function(e){
        $(this).val("");
    });
    
    initTip();
    
    /*MESSAGES*/
		//Alert
    $('div.msgbar').click(function(){
        $(this).slideUp();
    })    
	setTimeout("$('div.msgbar').slideUp()", 3500);
    
    /*Date Picker*/
    $( "#fechaDesde" ).datepicker();
    $( "#fechaHasta" ).datepicker();
});

   	function initTip()
	{
		jQuery('.tip_north').tipsy({gravity: 's'});
		jQuery('.tip_south').tipsy({gravity: 'n'});
		jQuery('.tip_east').tipsy({gravity: 'e'});
		jQuery('.tip_west').tipsy({gravity: 'w'});
	}
	
	function initPop()
	{
		jQuery("a#gallery_box").fancybox({
			'titlePosition' : 'inside'
		}); 
	}