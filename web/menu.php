<!--Dreamworks Container-->
<div id="dreamworks_container">
    <!--Primary Navigation-->
    <nav id="primary_nav">
        <ul>
            <li class="nav_dashboard <?php echo ($urlParams[0] == "principal")?"active":"";?>"><a href="/principal/index">Principal</a></li>
            <li class="nav_pages <?php echo ($urlParams[0] == "cotizacion")?"active":"";?>"><a href="/cotizacion/index">Cotizaciones</a></li>
            <li class="nav_forms <?php echo ($urlParams[0] == "factura")?"active":"";?>"><a href="/factura/index">Facturas</a></li>
            <li class="nav_forms"><a href="./forms.html">Forms</a></li>
            <li class="nav_typography"><a href="./typography.html">Typography</a></li>
            <li class="nav_uielements"><a href="./ui_elements.html">UI Elements</a></li>
            <li class="nav_pages"><a href="./pages.html">Pages</a></li>
        </ul>
    </nav>
<!--Main Content-->
<section id="main_content">
    <!--Secondary Navigation-->
    <nav id="secondary_nav"> 
    <!--UserInfo-->
    <dl class="user_info">
    	<?php
		$email = "jorgeperaza2000@gmail.com";
		$size = 79;
		$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?s=" . $size;
		?>
        <dt><a href="#"><img src="<?php echo $grav_url; ?>" alt="" /></a></dt>
        <dd>
        <a class="welcome_user" href="#">Welcome,<strong>John Doe</strong></a>
        <span class="log_data">Last sign in : 16:11 Feb 27th 2012</span>
        <a class="logout" href="./login.html">Logout</a>
        <a class="user_messages" href="#"><span>12</span></a>
        </dd>
    </dl>
    <?php if($urlParams[0] == "principal") { ?>
    <h2><?= ucwords($urlParams[0]);?></h2>
    <ul>
        <li><a href="#"><span class="iconsweet">a</span>Tareas pendientes</a></li>
        <li><a href="#"><span class="iconsweet">k</span>Nueva tarea</a></li>
    </ul>
    <?php } ?>
    <?php if($urlParams[0] == "cotizacion") { ?>
    <h2><?= ucwords($urlParams[0]);?></h2>
    <ul>
        <li><a href="/cotizacion/index"><span class="iconsweet">a</span>Buscar</a></li>
        <li><a href="/cotizacion/add"><span class="iconsweet">k</span>Nueva</a></li>
    </ul>
    <?php } ?>
    <?php if($urlParams[0] == "factura") { ?>
    <h2><?= ucwords($urlParams[0]);?></h2>
    <ul>
        <li><a href="#"><span class="iconsweet">a</span>Buscar</a></li>
        <li><a href="#"><span class="iconsweet">k</span>Nueva</a></li>
    </ul>
    <?php } ?>
    </nav>