<?php
function getCotizaciones($conditions = null)
{    
    $strSql = "SELECT 
                a.*,
                b.cliente
               FROM 
                cotizaciones a,
                clientes b
               WHERE 
                a.id_cliente = b.id_cliente
               " .
               $conditions 
               . " 
               ORDER BY
                fecha DESC 
               LIMIT 
                10";
    return getRegistros($strSql);
}

function findCotizacion($id = null)
{
    $strSql = "SELECT 
                a.*,
                b.cliente
               FROM 
                cotizaciones a,
                clientes b
               WHERE 
                a.id_cliente = b.id_cliente
               AND
                id_cotizacion = '" . $id . "' 
               ORDER BY
                fecha DESC 
               LIMIT 
                10";
    return getRegistros($strSql);
}

function getNumeroCotizaciones($codCotizacion = null) 
{
    $strSql = "SELECT 
                cod_cotizacion
               FROM 
                cotizaciones 
               WHERE 
                cod_cotizacion = '" . $codCotizacion . "'";
    return countRegistros($strSql);
}

function guardarCotizacion($fields = array(), $fieldsDb = array())
{
    $return = saveData($fields, $fieldsDb, 'cotizaciones');
    return $return;
}   

?>
