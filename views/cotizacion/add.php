<form name="frmCondicCotizaciones" method="post" action="/cotizacion/save">
<!--Botonera-->
<div class="botonera">
    <a class="button_submit right" onclick="document.frmCondicCotizaciones.submit();"><span class="iconsweet">+</span>Guardar</a>
</div>

<?php
    isset($datos["flash"])?flash($datos["flash"]):null;
    if (isset($_SESSION["flash"])){
        echo $_SESSION["flash"];
        unset($_SESSION["flash"]);
    }
?>
<!--Condiciones de busqueda-->
<div class="one_two_wrap fl_left">
    <div class="widget">
        <div class="widget_title"><span class="iconsweet">8</span><h5>Datos de la cotización</h5></div>
        <div class="widget_body">
            <table class="form_datatable" width="100%" border="0" cellspacing="0" cellpadding="6">
                <tr>
                    <td>Descripción</td>
                    <td><input type="text" size="30" name="txtDescripcion"></td>
                </tr>
                <tr>
                    <td>Razón Social</td>
                    <td>
                        <input size="5" value="" type="text" name="txtIdCliente">
                        <input type="text" name="txtRazonSocial">
                        <a class="tip_west all" href="#" original-title="Click para buscar y seleccionar clientes">
                            <span class="count">Buscar</span>
                        </a
                    </td>
                </tr>
                <tr>
                    <td>Persona Contacto</td>
                    <td><input type="text" name="txtPersonaContacto"></td>
                </tr>
                <tr>
                    <td>Moneda</td>
                    <td><input type="text" size="10" name="txtMoneda"></td>
                </tr>
                <tr>
                    <td>Gastos de Transporte</td>
                    <td><input type="text" size="10" name="txtGastoTransporte"></td>
                </tr>
                <tr>
                    <td>Gastos Adminnistrativos</td>
                    <td><input type="text" size="10" name="txtGastoAdministrativos"></td>
                </tr>
                <tr>
                    <td>Porcentaje Freelance</td>
                    <td><input type="text" size="10" name="txtPorcentajeFreelance"></td>
                </tr>
                <tr>
                    <td>Margen de Ganancia</td>
                    <td><input type="text" size="10" name="txtMargenGanancia"></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="one_two_wrap fl_right">
    <div class="widget">
        <div class="widget_title"><span class="iconsweet">}</span><h5>Datos generales</h5></div>
        <div class="widget_body">
            <table class="form_datatable" width="100%" border="0" cellspacing="0" cellpadding="6">
                <tr>
                    <td>Nro Cotización</td>
                    <td><input type="text" size="10" name="txtCodCotizacion"></td>
                </tr>
                <tr>
                    <td>Fecha</td>
                    <td><input size="10" type="text" name="txtFecha" id="fecha"></td>
                </tr>
                <tr>
                    <td>Iva</td>
                    <td><input type="text" size="5" name="txtIvaActual"></td>
                </tr>
                <tr>
                    <td>Tasa de Cambio</td>
                    <td><input type="text" size="5" name="txtTasaCambio"></td>
                </tr>
                <tr>
                    <td>SUMAT</td>
                    <td><input type="text" size="5" name="txtSumat"></td>
                </tr>
                <tr>
                    <td>ITF</td>
                    <td><input type="text" size="5" name="txtItf"></td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</div>
</form>