<!--Quick Actions-->
<div id="quick_actions">
    <a class="button_big" href="/cotizacion/add"><span class="iconsweet">+</span>Nueva Cotizacion</a>
    <a class="button_big btn_grey" href="#"><span class="iconsweet">f</span>Manage Projects</a>
</div>

<!--Condiciones de busqueda-->
<div class="one_two_wrap fl_left">
    <div class="widget">
        <div class="widget_title">
            <span class="iconsweet">r</span>
            <h5>Criterios de Búsqueda</h5>
            <a class="tip_west greyishBtn button_small right" href="#" style="margin:5px;" original-title="Para limpiar los campos de búsqueda haga doble-click sobre ellos.">?</a></div>
        <div class="widget_body">
            <form name="frmCondicCotizaciones" method="post" action="<?=$_SERVER["REQUEST_URI"];?>">
            <table class="form_datatable" width="100%" border="0" cellspacing="0" cellpadding="8">
                <tr>
                    <td>Nro Cotización</td>
                    <td><input type="text" name="txtNumeroCotizacion" class="borrable" value="<?=isset($_POST["txtNumeroCotizacion"])?$_POST["txtNumeroCotizacion"]:null;?>"></td>
                </tr>
                <tr>
                    <td>Fecha</td>
                    <td>
                        Desde: <input size="10" type="text" class="borrable" name="txtFechaDesde" id="fechaDesde"> 
                        Hasta: <input size="10" type="text" class="borrable" name="txtFechaHasta" id="fechaHasta">
                    </td>
                </tr>
                <tr>
                    <td>Descripción</td>
                    <td><input type="text" class="borrable" name="txtDescripcion"></td>
                </tr>
                <tr>
                    <td>Cliente</td>
                    <td><input type="text" class="borrable" name="txtCliente"></td>
                </tr>
                <tr>
                    <td>Estatus</td>
                    <td>
                        Abierta <input type="checkbox" name="txtAbierta" value="Abierta">
                        Cerrada <input type="checkbox" name="txtCerrada" value="Cerrada">
                        Activa <input type="checkbox" name="txtActiva" value="Activa">
                        Descartada <input type="checkbox" name="txtDescartada" value="Descartada">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right"><input class="greyishBtn button_small" type="submit" value="Filtrar"></td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</div>

<!--One_Wrap-->
<div class="one_wrap">
    <div class="widget">
        <div class="widget_title"><span class="iconsweet">f</span><h5>Cotizaciones</h5></div>
        <div class="widget_body">
            <!--Activity Table-->
            <table class="activity_datatable" width="100%" border="0" cellspacing="0" cellpadding="8">
                <tr>
                    <th width="3%">Nro</th>
                    <th width="28%">Descripción</th>
                    <th width="30%">Cliente</th>
                    <th width="12%">Fecha</th>
                    <th width="10%">Relación</th>
                    <th width="10%">Estatus</th>
                    <th width="7%">Acciones</th>
                </tr>
                <?php
                if (is_array($datos["cotizaciones"])) {
                foreach($datos["cotizaciones"] as $dato):
                ?>
                    <tr>
                        <td><span class="green_highlight pj_cat"><?=$dato["cod_cotizacion"];?></span></td>
                        <td><?=$dato["descripcion"];?></td>
                        <td><?=$dato["cliente"];?></td>
                        <td><?=$dato["fecha"];?></td>
                        <td><?=$dato["relacion"];?></td>
                        <td><?=$dato["status"]."-".$dato["status1"];?></td>
                        <td><span class="data_actions iconsweet"><a class="tip_north" original-title="User" href="#">a</a> <a class="tip_north" original-title="Edit" href="#">C</a> 						<a class="tip_north" original-title="Delete" href="#">X</a></span></td>
                    </tr>
                <?php
                endforeach;
                } else {
                ?>
                    <tr>
                        <td colspan="7" align="center">No se encontraron registros</td>
                    </tr>
                <?php } ?>
            </table>

            <div class="action_bar">
                <a class="button_small whitishBtn" href="#"><span class="iconsweet">l</span>Exportar Reporte</a>
            </div>