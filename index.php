<?php 
session_start();
$_SESSION["url"] = null;

include_once 'core/global.php';
include_once 'core/database.php';
$urlParams = getUrl();

if (is_file('models/' . $urlParams[0] . '.php')) {
    include_once 'models/' . $urlParams[0] . '.php';
} else {
    echo "No se encuentra el modelo.";
}

if (is_file('controllers/' . $urlParams[0] . 'Controller.php')) {
    include_once 'controllers/' . $urlParams[0] . 'Controller.php';
} else {
    echo "No se encuentra el controlador.";
}

if(is_callable($urlParams[1])){
    $urlParams[1]();
} else {
    echo "No se encuentra el metodo." . $urlParams[1];
}

include_once 'web/header.php';
include_once 'web/menu.php';
include_once 'web/title.php';

if (isset($_SESSION["view"])) {
    include_once $_SESSION["view"];
}

include_once 'web/footer.php';
